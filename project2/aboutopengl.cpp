#include "aboutopengl.h"
#include "ui_aboutopengl.h"
#include "gl.h"

AboutOpenGL::AboutOpenGL(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutOpenGL)
{
    ui->setupUi(this);

    QString vendor = QString::fromUtf8((const char*) glGetString(GL_VENDOR));
    QString renderer = QString::fromUtf8((const char*) glGetString(GL_RENDERER));
    QString version = QString::fromUtf8((const char*) glGetString(GL_VERSION));
    QString shaderVersion = QString::fromUtf8((const char*) glGetString(GL_SHADING_LANGUAGE_VERSION));
    QString extensions;  //QString::fromUtf8((const char*) glGetString(GL_EXTENSIONS));
    GLint n, i;
    glGetIntegerv(GL_NUM_EXTENSIONS, &n);
    for(i = 0; i < n; i++)
    {
        extensions += QString::fromUtf8((const char*) glGetStringi(GL_EXTENSIONS, i));
        if(i < n -1)
            extensions += "\n";
    }
    ui->lblVendorValue->setText(vendor);
    ui->lblRendererValue->setText(renderer);
    ui->lblVersionValue->setText(version);
    ui->lblShadingLanguageValue->setText(shaderVersion);
    ui->tbGLExtensionsValue->setPlainText(extensions);
    ui->tbGLExtensionsValue->setReadOnly(true);
    layout()->setSizeConstraint( QLayout::SetFixedSize );
}

AboutOpenGL::~AboutOpenGL()
{
    delete ui;
}

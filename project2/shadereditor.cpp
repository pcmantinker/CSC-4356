#include "shadereditor.h"
#include "ui_shadereditor.h"
#include <QtGui>

ShaderEditor::ShaderEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShaderEditor)
{
    ui->setupUi(this);
    mainwindow = (MainWindow*) parent;
}

ShaderEditor::~ShaderEditor()
{
    delete ui;
}

void ShaderEditor::on_btnClose_clicked()
{
    hide();
}

void ShaderEditor::on_btnCompile_clicked()
{
    // code for compiling shader
    mainwindow->glWidget()->customShader(ui->tbVertShader->toPlainText(), ui->tbFragShader->toPlainText(), "custom");
}

void ShaderEditor::on_btnSave_clicked()
{

    QString selectedFilter;
    QString fileName;

    fileName = QFileDialog::getSaveFileName(this,
                                            tr("Save vertex shader..."),
                                            QString("vertex_shader"),
                                            tr("Vertex Shader (*.vert)"),
                                            &selectedFilter);
    saveShaderFile(fileName, ui->tbVertShader->toPlainText());

    fileName = QFileDialog::getSaveFileName(this,
                                            tr("Save shader..."),
                                            QString("fragment_shader"),
                                            tr("Fragment Shader (*.frag)"),
                                            &selectedFilter);
    saveShaderFile(fileName, ui->tbFragShader->toPlainText());
}

void ShaderEditor::saveShaderFile(QString file, QString text)
{
    QFile f(file);
    f.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&f);
    out << text;

    f.close();
    QMessageBox msgBox;
    msgBox.setText(file + " successfully saved!");
    msgBox.setWindowTitle("Success");
    msgBox.exec();
}


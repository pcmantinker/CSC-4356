#include <QtGui>
#include "gl.h"
//#include <QtOpenGL>

#include <math.h>

#include "glwidget.h"
#include <time.h>

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    m_obj = 0;
    ShaderProgram = new QGLShaderProgram();
    QTimer * timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateGL()));
    timer->setInterval(10);
    timer->start();
    et = new QElapsedTimer();
    et->start();

    sizeX = 0.5f;
    sizeY = 0.5f;
    fractionX = 0.5f;
    fractionY = 0.5f;
    mortar_color = new QVector3D(1.0f, 1.0f, 1.0f);
    brick_color = new QVector3D(1.0f, 0.0f, 0.0f);
    brick_size = new QVector2D(sizeX, sizeY);
    brick_frac = new QVector2D(fractionX, fractionY);
}

GLWidget::~GLWidget()
{

}

void GLWidget::setObj(obj *o)
{
    rotation_x = 0.0f;
    rotation_y = 0.0f;
    position_z = 5.0f;
    scale      = 1.0f;
    m_obj = o;
    updateGL();
}

QString GLWidget::currentShader()
{
    return m_currentShader;
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(800, 800);
}

void GLWidget::initializeGL()
{
    if (glewInit() == GLEW_OK)
    {
        qDebug() << "GLEW_OK";
    }
    else
    {
        qDebug() << "GLEW not loaded";
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_NORMALIZE);
    glEnable(GL_SMOOTH);

    // ambient light
    GLfloat ambColor[] = {0.2f, 0.2f, 0.2f, 1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambColor);

    // positioned light
    GLfloat lColor0[] = {0.5f, 0.5f, 0.5f, 1.0f};
    GLfloat lPos0[] = {4.0f, 0.0f, 8.0f, 1.0f};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lColor0);
    glLightfv(GL_LIGHT0, GL_POSITION, lPos0);

    GLfloat lColor1[] = {0.5f, 0.2f, 0.2f, 1.0f};
    GLfloat lPos1[] = {-1.0f, 0.5f, 0.5f, 0.0f};
    glLightfv(GL_LIGHT1, GL_DIFFUSE, lColor1);
    glLightfv(GL_LIGHT1, GL_POSITION, lPos1);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

void GLWidget::paintGL()
{
    qDebug() << "paintGL()";

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslated(0.0, 0.0, -position_z);
    glRotated(rotation_x, 1.0, 0.0, 0.0);
    glRotated(rotation_y, 0.0, 1.0, 0.0);
    glScaled(scale, scale, scale);

    if(m_obj != 0)
    {
        qDebug() << "Drawing obj";
        obj_render(m_obj);
    }

    float elapsedTime = et->elapsed() / 1000.0f;
    qDebug() << elapsedTime;

    if(ShaderProgram->isLinked())
        ShaderProgram->setUniformValue("time", elapsedTime);
}

void GLWidget::waveShader()
{
    QFile vert(":/wave_shader.vert");
    vert.open(QFile::ReadOnly);
    QString vertText = vert.readAll();
    vert.close();
    qDebug() << vertText;

    QFile frag(":/simplest.frag");
    frag.open(QFile::ReadOnly);
    QString fragText = frag.readAll();
    frag.close();
    qDebug() << fragText;

    customShader(vertText, fragText, "wave");

    updateGL();
}

void GLWidget::brickShader()
{
    QFile vert(":/brick_shader.vert");
    vert.open(QFile::ReadOnly);
    QString vertText = vert.readAll();
    vert.close();
    qDebug() << vertText;

    QFile frag(":/brick_shader.frag");
    frag.open(QFile::ReadOnly);
    QString fragText = frag.readAll();
    frag.close();
    qDebug() << fragText;

    customShader(vertText, fragText, "brick");

    ShaderProgram->setUniformValue("mortar_color", mortar_color->x(), mortar_color->y(), mortar_color->z());
    ShaderProgram->setUniformValue("brick_color", brick_color->x(), brick_color->y(), brick_color->z());
    ShaderProgram->setUniformValue("brick_size", brick_size->x(), brick_size->y());
    ShaderProgram->setUniformValue("brick_frac", brick_frac->x(), brick_frac->y());

    updateGL();
}

void GLWidget::customShader(QString vertText, QString fragText, QString shaderName)
{
    m_currentShader = shaderName;
    //    delete ShaderProgram;
    //    ShaderProgram = new QGLShaderProgram();
    ShaderProgram->release();
    ShaderProgram->removeAllShaders();

    VertexShader = new QGLShader(QGLShader::Vertex);
    if(VertexShader->compileSourceCode(vertText))
    {
        ShaderProgram->addShader(VertexShader);
        qDebug() << "Vertex Shader Compiled: " << VertexShader->isCompiled();
    }
    else
        QMessageBox::critical(this, "GLSL Vertex Shader Compiler Error", VertexShader->log());


    FragmentShader = new QGLShader(QGLShader::Fragment);
    if(FragmentShader->compileSourceCode(fragText))
    {
        ShaderProgram->addShader(FragmentShader);
        qDebug() << "Fragment Shader Compiled: " << FragmentShader->isCompiled();
    }
    else
        QMessageBox::critical(this, "GLSL Fragment Shader Compiler Error", FragmentShader->log());

    if(!ShaderProgram->link())
    {
        QMessageBox::critical(this, "GLSL Linker Error", "There was an error linking the program: \n" + ShaderProgram->log());
    }
    else
        ShaderProgram->bind();

    updateGL();
}

void GLWidget::resizeGL(int width, int height)
{  
    GLdouble tb = 0.5;
    GLdouble lr = 0.5 * (GLdouble) width / (GLdouble) height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-lr, lr, -tb, tb, 1.0, 100.0);

    glViewport(0, 0, width, height);
    glMatrixMode(GL_MODELVIEW);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    click_nx = (GLdouble) event->x() / width();
    click_ny = (GLdouble) event->y() / height();
    click_rotation_x = rotation_x;
    click_rotation_y = rotation_y;
    click_scale      = scale;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    GLdouble nx = (GLdouble) event->x() / width();
    GLdouble ny = (GLdouble) event->y() / height();
    GLdouble dx = nx - click_nx;
    GLdouble dy = ny - click_ny;

    if(event->buttons() & Qt::LeftButton)
    {
        rotation_x = click_rotation_x + 90.0 * dy;
        rotation_y = click_rotation_y + 180.0 * dx;

        if (rotation_x >   90.0) rotation_x  =  90.0;
        if (rotation_x <  -90.0) rotation_x  = -90.0;
        if (rotation_y >  180.0) rotation_y -= 360.0;
        if (rotation_y < -180.0) rotation_y += 360.0;
    }

    if(event->buttons() & Qt::RightButton)
    {
        scale = click_scale - dy;
    }

    qDebug() << "dx: " << dx;
    qDebug() << "dy: " << dy;
    qDebug() << "Rotation x: " << rotation_x;
    qDebug() << "Rotation y: " << rotation_y;
    qDebug() << "Scale: " << scale;
    updateGL();
}

void GLWidget::setMortarColor(QVector3D *color)
{
    mortar_color = color;
    ShaderProgram->setUniformValue("mortar_color", mortar_color->x(), mortar_color->y(), mortar_color->z());
    updateGL();
}

void GLWidget::setBrickColor(QVector3D *color)
{
    brick_color = color;
    ShaderProgram->setUniformValue("brick_color", brick_color->x(), brick_color->y(), brick_color->z());
    updateGL();
}

void GLWidget::setBrickSize(QVector2D *size)
{
    brick_size = size;
    ShaderProgram->setUniformValue("brick_size", brick_size->x(), brick_size->y());
    updateGL();
}

void GLWidget::setBrickFraction(QVector2D *fraction)
{
    brick_frac = fraction;
    ShaderProgram->setUniformValue("brick_frac", brick_frac->x(), brick_frac->y());
    updateGL();
}

QVector3D *GLWidget::brickColor()
{
    return brick_color;
}

QVector3D *GLWidget::mortarColor()
{
    return mortar_color;
}

QVector2D *GLWidget::brickSize()
{
    return brick_size;
}

QVector2D *GLWidget::brickFraction()
{
    return brick_frac;
}

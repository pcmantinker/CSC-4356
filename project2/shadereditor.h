#ifndef SHADEREDITOR_H
#define SHADEREDITOR_H

#include <QDialog>
#include <QTextEdit>
#include "mainwindow.h"

namespace Ui {
class ShaderEditor;
}

class ShaderEditor : public QDialog
{
    Q_OBJECT
    
public:
    explicit ShaderEditor(QWidget *parent = 0);
    ~ShaderEditor();
    
private slots:
    void on_btnClose_clicked();

    void on_btnCompile_clicked();

    void on_btnSave_clicked();

private:
    Ui::ShaderEditor *ui;
    QString m_shaderType;
    void saveShaderFile(QString file, QString text);
    MainWindow *mainwindow;
};

#endif // SHADEREDITOR_H

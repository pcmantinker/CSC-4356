#-------------------------------------------------
#
# Project created by QtCreator 2012-10-21T15:45:04
#
#-------------------------------------------------

QT       += core gui opengl

TARGET = Project2
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    about.cpp \
    glwidget.cpp \
    obj.c \
    image.c \
    shadereditor.cpp \
    shadermanagement.cpp \
    brickshaderoptions.cpp \
    aboutopengl.cpp

HEADERS  += mainwindow.h \
    about.h \
    glwidget.h \
    obj.h \
    image.h \
    gl.h \
    shadereditor.h \
    shadermanagement.h \
    brickshaderoptions.h \
    aboutopengl.h

FORMS    += mainwindow.ui \
    shadereditor.ui \
    shadermanagement.ui \
    brickshaderoptions.ui \
    aboutopengl.ui

unix:!macx {
    LIBS += -lglut -lGLEW -lGL -ljpeg -lpng -ltiff -lz -lm
}

macx {
    LIBS += -lGLEW -framework OpenGL -ljpeg -ltiff -lz -lm
    LIBS += /usr/X11/lib/libpng.dylib
    INCLUDEPATH += /usr/X11/include/
    CONFIG += app_bundle
}

win32 {
    LIBS += -lfreeglut -lglew32 -lopengl32 -lwinmm -lgdi32 -ljpeg -lpng -ltiff -lz -lm
}

RESOURCES += \
    icons.qrc \
    builtin_shaders.qrc

OTHER_FILES += \
    myapp.rc \
    shaders/someshader/someshader.vert \
    shaders/someshader/someshader.frag

RC_FILE = myapp.rc

#ifdef __APPLE__
#include <GL/glew.h>
#include <GLUT/glut.h>
#else
#include <GL/glew.h>
#include <GL/glut.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

GLdouble rotation_x;
GLdouble rotation_y;
GLdouble position_z;
GLdouble scale;

int      click_button;
GLdouble click_rotation_x;
GLdouble click_rotation_y;
GLdouble click_scale;                                                          
GLdouble click_nx;
GLdouble click_ny;

FILE *fp;
char *fileName;
int vertices = 0;
int faces = 0;

typedef struct vert vert;
typedef struct elem elem;

struct vert
{
    GLfloat v[3];
    GLfloat n[3];
};

struct elem
{
	GLushort i[3];
};

/* dynamically allocated arrays as determined by the number of vertices and elems */
vert *verts;
elem *elems;

static void readFile(char file[])
{
	int close = 0;
	int i = 0;
	
	if(file == NULL)
	{
		fprintf(stderr, "Usage: project1 file.mesh\n");
		exit(1);
	}
	printf("Opening %s...\n", file);
	fp = fopen(file, "r");
	if(fp == NULL)
	{
		fprintf(stderr, "Can't open the file %s!\n", file);
		exit(1);
	}	
	
	/* read all strings from the file that are not white space */	
	fscanf(fp, "%d %d\n", &vertices, &faces);
	printf("Vertices: %d \nFaces: %d\n", vertices, faces);
	/* dynamically allocate the array for vertices based on how many vertices are defined in the first line */
	verts = (vert *) malloc(vertices * sizeof(vert));
	/* dynamically allocate the array for elems based on how many faces are defined in the first line */
	elems = (elem *) malloc(faces * sizeof(elem));
	printf("Allocated %d bytes for verts.\n", vertices * sizeof(vert));
	printf("Allocated %d bytes for elems.\n", faces * sizeof(elem));	
	
	for(i = 0; i < vertices; i++)
	{
		vert vertex;
		fscanf(fp, "v %f %f %f %f %f %f\n", &vertex.v[0], &vertex.v[1], &vertex.v[2], &vertex.n[0], &vertex.n[1], &vertex.n[2]);
		verts[i] = vertex;
	}
	
	for(i = 0; i < faces; i++)
	{
		elem face;
		fscanf(fp, "t %d %d %d\n", &face.i[0], &face.i[1], &face.i[2]);
		elems[i] = face;
	}
	
	/*
	printf("Output of arrays: \n");

	for(i = 0; i < vertices; i++)
	{
		printf("verts[%d].v[0] = %4.5f \t verts[%d].v[1] = %4.5f \t verts[%d].v[2] = %4.5f\nverts[%d].n[0] = %4.5f \t verts[%d].n[1] = %4.5f \t verts[%d].n[2] = %4.5f\n", i, verts[i].v[0], i, verts[i].v[1], i, verts[i].v[2], i, verts[i].n[0], i, verts[i].n[1], i, verts[i].n[2]);
	}
	
	for(i = 0; i < faces; i++)
	{
		printf("elems[%d].i[0] = %d\telems[%d].i[1] = %d\telems[%d].i[2] = %d\n", i, elems[i].i[0], i, elems[i].i[1], i, elems[i].i[2]);
	}
	*/
	
	close = fclose(fp);
	
	if(close==0)
	{
		printf("File, %s, closed successfully!\n", file);
	}
	else
	{
		fprintf(stderr, "Error closing %s!\n", file);
	}
}

static void processMesh(char fileName[])
{
	readFile(fileName);
}

void startup(void)
{
    rotation_x = 0.0;
    rotation_y = 0.0;
    position_z = 5.0;
    scale      = 1.0;                                                          \

    glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE);
	glEnable(GL_SMOOTH);
	
		// ambient light
	GLfloat ambColor[] = {0.2f, 0.2f, 0.2f, 1.0f};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambColor);
	
	// positioned light
	GLfloat lColor0[] = {0.5f, 0.5f, 0.5f, 1.0f};
	GLfloat lPos0[] = {4.0f, 0.0f, 8.0f, 1.0f};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lColor0);
	glLightfv(GL_LIGHT0, GL_POSITION, lPos0);
	
	GLfloat lColor1[] = {0.5f, 0.2f, 0.2f, 1.0f};
	GLfloat lPos1[] = {-1.0f, 0.5f, 0.5f, 0.0f};
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lColor1);
	glLightfv(GL_LIGHT1, GL_POSITION, lPos1);

    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
}

static void renderModel()
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof (vert) * vertices, verts, GL_STATIC_DRAW);
	
	GLuint ebo;
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (elem) * faces, elems, GL_STATIC_DRAW);
	
	size_t s = sizeof (GLfloat);
	glVertexPointer(3, GL_FLOAT, s * 6, (GLvoid *) ( 0));
	glNormalPointer( GL_FLOAT, s * 6, (GLvoid *) (s * 3));
	
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	
	glDrawElements(GL_TRIANGLES, faces*3, GL_UNSIGNED_SHORT, 0);
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}

static void reshape(int w, int h)
{
    GLdouble tb = 0.5;
    GLdouble lr = 0.5 * (GLdouble) w / (GLdouble) h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-lr, lr, -tb, tb, 1.0, 100.0);

    glViewport(0, 0, w, h);
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	
	glTranslated(0.0, 0.0, -position_z);
    glRotated(rotation_x, 1.0, 0.0, 0.0);
    glRotated(rotation_y, 0.0, 1.0, 0.0);
    glScaled(scale, scale, scale);    
	
	renderModel();
    glutSwapBuffers();
}

void motion(int x, int y)
{
    GLdouble nx = (GLdouble) x / glutGet(GLUT_WINDOW_WIDTH);
    GLdouble ny = (GLdouble) y / glutGet(GLUT_WINDOW_HEIGHT);

    GLdouble dx = nx - click_nx;
    GLdouble dy = ny - click_ny;

    if (click_button == GLUT_LEFT_BUTTON)
    {
        rotation_x = click_rotation_x +  90.0 * dy;
        rotation_y = click_rotation_y + 180.0 * dx;

        if (rotation_x >   90.0) rotation_x  =  90.0;
        if (rotation_x <  -90.0) rotation_x  = -90.0;
        if (rotation_y >  180.0) rotation_y -= 360.0;
        if (rotation_y < -180.0) rotation_y += 360.0;
    }
    if (click_button == GLUT_RIGHT_BUTTON)
    {
        scale = click_scale - dy;                                              \
    }

    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
    click_nx = (GLdouble) x / glutGet(GLUT_WINDOW_WIDTH);
    click_ny = (GLdouble) y / glutGet(GLUT_WINDOW_HEIGHT);

    click_button     = button;
    click_rotation_x = rotation_x;
    click_rotation_y = rotation_y;
    click_scale      = scale;
}

int main(int argc, char *argv[])
{
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(640, 480);
    glutInit(&argc, argv);

	processMesh(argv[1]);
    glutCreateWindow(argv[0]);	
	
	printf("VENDOR     = %s\n", glGetString(GL_VENDOR));
    printf("RENDERER   = %s\n", glGetString(GL_RENDERER));
    printf("VERSION    = %s\n", glGetString(GL_VERSION));
    printf("EXTENSIONS = %s\n", glGetString(GL_EXTENSIONS));

	glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutMotionFunc(motion);
    glutMouseFunc(mouse);

    if (glewInit() == GLEW_OK)
    {
        startup();
        glutMainLoop();
    }

    return 0;
}

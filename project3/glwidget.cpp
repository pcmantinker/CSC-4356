#include <QtGui>
#include "gl.h"
//#include <QtOpenGL>

#include <math.h>

#include "glwidget.h"
#include <time.h>

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    m_obj = 0;
    ShaderProgram = new QGLShaderProgram();
    QTimer * timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateGL()));
    timer->setInterval(10);
    timer->start();
    et = new QElapsedTimer();
    et->start();

}

GLWidget::~GLWidget()
{

}

void GLWidget::setObj(obj *o)
{
    rotation_x = 0.0f;
    rotation_y = 0.0f;
    position_z = 5.0f;
    scale      = 1.0f;
    m_obj = o;
    updateGL();
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(800, 800);
}

void GLWidget::initializeGL()
{
    if (glewInit() == GLEW_OK)
    {
        qDebug() << "GLEW_OK";
    }
    else
    {
        qDebug() << "GLEW not loaded";
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_SMOOTH);

    GLfloat DiffuseMaterial[] = {1.0, 1.0, 1.0};
    GLfloat AmbientMaterial[] = {0.0, 0.0, 0.0};
    GLfloat SpecularMaterial[] = {1.0, 1.0, 1.0};

    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, DiffuseMaterial);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, AmbientMaterial);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, SpecularMaterial);

    // positioned light
    GLfloat lColor0[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat lPos0[] = {0.0f, 0.0f, 1.0f, 0.0f};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lColor0);
    glLightfv(GL_LIGHT0, GL_POSITION, lPos0);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslated(0.0, 0.0, -position_z);
    glRotated(rotation_x, 1.0, 0.0, 0.0);
    glRotated(rotation_y, 0.0, 1.0, 0.0);
    glScaled(scale, scale, scale);

    float elapsedTime = et->elapsed() / 1000.0f;

    if(m_obj != 0)
    {
        if(ShaderProgram->isLinked())
        {
            ShaderProgram->setUniformValue("time", elapsedTime);
            //            ShaderProgram->setUniformValue("mouseX", spotlight_x);
            //            ShaderProgram->setUniformValue("mouseY", spotlight_y);
            load13BoxTextures();
        }
        else
        {
            ShaderProgram->bindAttributeLocation("tangent", 6);
            loadShaderProgram();
        }

        obj_render(m_obj);
        obj_render_axes(m_obj, 0.3f);
    }


}

void GLWidget::load13BoxTextures()
{
    diffuse = obj_get_mtrl_map(m_obj, 0, OBJ_KD);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuse);
    ShaderProgram->setUniformValue("diffuse", 0);

    normal = obj_get_mtrl_map(m_obj, 0, OBJ_KA);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, normal);
    ShaderProgram->setUniformValue("normal", 1);

    specular = obj_get_mtrl_map(m_obj, 0, OBJ_KS);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, specular);
    ShaderProgram->setUniformValue("specular", 2);

    QImage image;
    image.load("spotlight.png");
    QImage glSpotlightTexture = QGLWidget::convertToGLFormat(image);
    glActiveTexture(GL_TEXTURE3);
    glGenTextures(1, &spot);
    glBindTexture(GL_TEXTURE_2D, spot);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, glSpotlightTexture.width(), glSpotlightTexture.height(),  GL_RGBA, GL_UNSIGNED_BYTE, glSpotlightTexture.bits() );
}

void GLWidget::loadShaderProgram()
{
    QFile vert(":/texture.vert");
    vert.open(QFile::ReadOnly);
    QString vertText = vert.readAll();
    vert.close();

    QFile frag(":/texture.frag");
    frag.open(QFile::ReadOnly);
    QString fragText = frag.readAll();
    frag.close();

    customShader(fragText, vertText);
}

void GLWidget::customShader(QString fragText, QString vertText)
{   if(ShaderProgram->isLinked())
    {
        ShaderProgram->release();
        ShaderProgram->removeAllShaders();
    }

    VertexShader = new QGLShader(QGLShader::Vertex);
    if(VertexShader->compileSourceCode(vertText))
    {
        ShaderProgram->addShader(VertexShader);
        qDebug() << "Vertex Shader Compiled: " << VertexShader->isCompiled();
    }
    else
        QMessageBox::critical(this, "GLSL Vertex Shader Compiler Error", VertexShader->log());


    FragmentShader = new QGLShader(QGLShader::Fragment);
    if(FragmentShader->compileSourceCode(fragText))
    {
        ShaderProgram->addShader(FragmentShader);
        qDebug() << "Fragment Shader Compiled: " << FragmentShader->isCompiled();
    }
    else
        QMessageBox::critical(this, "GLSL Fragment Shader Compiler Error", FragmentShader->log());

    if(!ShaderProgram->link())
    {
        QMessageBox::critical(this, "GLSL Linker Error", "There was an error linking the program: \n" + ShaderProgram->log());
    }
    else
        ShaderProgram->bind();

    updateGL();
}

void GLWidget::resizeGL(int width, int height)
{  
    GLdouble tb = 0.5;
    GLdouble lr = 0.5 * (GLdouble) width / (GLdouble) height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-lr, lr, -tb, tb, 1.0, 100.0);

    glViewport(0, 0, width, height);
    glMatrixMode(GL_MODELVIEW);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    click_nx = (GLdouble) event->x() / width();
    click_ny = (GLdouble) event->y() / height();
    click_rotation_x = rotation_x;
    click_rotation_y = rotation_y;
    click_scale      = scale;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    nx = (GLdouble) event->x() / width();
    ny = (GLdouble) event->y() / height();
    GLdouble dx = nx - click_nx;
    GLdouble dy = ny - click_ny;

    spotlight_x = nx + 10.0 * dy;
    spotlight_y = ny + 10.0 * dx;

    if(event->buttons() & Qt::LeftButton)
    {
        rotation_x = click_rotation_x + 90.0 * dy;
        rotation_y = click_rotation_y + 180.0 * dx;

        if (rotation_x >   90.0) rotation_x  =  90.0;
        if (rotation_x <  -90.0) rotation_x  = -90.0;
        if (rotation_y >  180.0) rotation_y -= 360.0;
        if (rotation_y < -180.0) rotation_y += 360.0;
    }

    if(event->buttons() & Qt::RightButton)
    {
        scale = click_scale - dy;
    }

    qDebug() << "dx: " << dx;
    qDebug() << "dy: " << dy;
    qDebug() << "nx: " << nx;
    qDebug() << "ny: " << ny;
    qDebug() << "spotlight_x: " << spotlight_x;
    qDebug() << "spotlight_y: " << spotlight_y;
    qDebug() << "Rotation x: " << rotation_x;
    qDebug() << "Rotation y: " << rotation_y;
    qDebug() << "Scale: " << scale;

    updateGL();
}

#include "about.h"

About::About(QWidget *parent) :  QDialog(parent)
{
    setWindowTitle("About");
    QLabel* description = new QLabel();
    description->setText("Description: CSC 4356 Project 3 - Open GL Shader/Texture demo<br /><br />"
                         "<a href='http://www.cameronjtinker.com/page/CSC-4356-Project-3.aspx'>Project Page</a> on my website <br />"
                         "Portions of this code are copyright Robert Kooima (<a href='http://csc.lsu.edu/~kooima/util3d/obj.html'>obj.c</a> and <a href='http://csc.lsu.edu/~kooima/util3d/image.html'>image.c</a>).<br />"
                         "Other portions of the code derived from Nokia examples.<br />"
                         "Wavefront objects copyright of their repsective owners.<br />"
                         "All models demoed are freeware.");
    description->setOpenExternalLinks(true);
    QPushButton *ok = new QPushButton();
    connect(ok, SIGNAL(clicked()), this, SLOT(okClicked()));
    ok->setText("OK");
    ok->setStyleSheet("text-align: center;");
    ok->setMaximumWidth(50);
    QVBoxLayout* textLayout = new QVBoxLayout();
    textLayout->addWidget(description);
    textLayout->addWidget(ok);
    QHBoxLayout* mainLayout = new QHBoxLayout();
    mainLayout->addLayout(textLayout);
    setLayout(mainLayout);

    QRect frect = frameGeometry();
    frect.moveCenter(QDesktopWidget().availableGeometry().center());
}

bool About::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress) {
        close();
        return true;
    } else {
        return false;
    }
}

void About::okClicked()
{
    close();
}

About::~About()
{

}

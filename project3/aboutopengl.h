#ifndef ABOUTOPENGL_H
#define ABOUTOPENGL_H

#include <QDialog>

namespace Ui {
class AboutOpenGL;
}

class AboutOpenGL : public QDialog
{
    Q_OBJECT
    
public:
    explicit AboutOpenGL(QWidget *parent = 0);
    ~AboutOpenGL();
    
private:
    Ui::AboutOpenGL *ui;
};

#endif // ABOUTOPENGL_H

varying vec4 var_L;
varying vec4 var_V;

attribute vec3 tangent;

void main()
{
    gl_Position = ftransform(); // Vertex position
    gl_TexCoord[0] = gl_MultiTexCoord0; // Texture coordinate
    gl_TexCoord[1] = gl_MultiTexCoord1; // Texture coordinate
    gl_TexCoord[2] = gl_MultiTexCoord2; // Texture coordinate
    gl_TexCoord[3] = gl_MultiTexCoord3; // Texture coordinate

    var_V = gl_ModelViewMatrix * gl_Vertex; // View vector
    var_L = gl_LightSource[0].position; // Light vector

    vec3 n = normalize(gl_NormalMatrix * gl_Normal);
    vec3 t = gl_NormalMatrix * tangent;
    vec3 b = normalize(cross(n, t));

    var_L = vec4(var_L.xyz * mat3(t, b, n), 1.0);
}
